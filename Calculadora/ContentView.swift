//
//  ContentView.swift
//  Calculadora
//
//  Created by Miguel Fernandez on 21/05/21.
//

import SwiftUI

enum CalcularBoton: String{
    
    case cero, uno, dos, tres, cuatro, cinco, seis, siete, ocho, nueve
    case igual, suma, resta, multiplicacion, division
    case decimal
    case ac, sumaresta, porcentaje
    
    var titulo: String {
        
        switch self {
        case .cero: return "0"
        case .uno: return "1"
        case .dos: return "2"
        case .tres: return "3"
        case .cuatro: return "4"
        case .cinco: return "5"
        case .seis: return "6"
        case .siete: return "7"
        case .ocho: return "8"
        case .nueve: return "9"
        case .suma: return "+"
        case .resta: return "-"
        case .multiplicacion: return "X"
        case .division: return "/"
        case .sumaresta: return "+/-"
        case .porcentaje: return "%"
        case .igual: return "="
        case .decimal: return "."
        default:
            return "AC"
        }
    }
    
    var colorDelBoton: Color {
        switch self {
        case .cero, .uno, .dos, .tres, .cuatro, .cinco, .seis, .siete, .ocho, .nueve:
            return Color(.darkGray)
            case .ac, .sumaresta, .porcentaje:
                return Color(.lightGray)
            default:
                return .orange
                
        }
        }
           }
    
enum Operacion {
    case suma, resta, multiplicacion, divicion, nada
}

struct ContentView: View {
    
    @State var valorDeLaEtiqueta = "0"
    @State var ejecutar: Operacion = .nada
    @State var numeroConsecutivo: Double = 0
    
    let botones:[[CalcularBoton]] = [
        [.ac, .sumaresta, .porcentaje, .division],
        [.siete, .ocho, .nueve, .multiplicacion],
        [.cuatro, .cinco, .seis, .resta],
        [.uno, .dos, .tres, .suma],
        [.cero, .decimal, .igual]
    
    ]
    
    var body: some View {
        
        ZStack(alignment: .bottom){
            Color.black.edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
            
            VStack(spacing: 12){
                
                HStack{
                    Spacer()
                    Text(valorDeLaEtiqueta)
                        .foregroundColor(.white)
                        .font(.system(size: 100))
                    
                }.padding()
                
                ForEach(botones, id: \.self){
                    row in
                    HStack{
                        ForEach(row, id: \.self){
                            boton in
                            
                            //Accion del Boton
                            Button(action: {
                                self.capturar(tecla: boton)
                                
                                
                            }) {
                                Text(boton.titulo)
                                    .font(.system(size: 32))
                                    .frame(width: self.AnchoDelBoton(botondelafuncion: boton), height: (UIScreen.main.bounds.width - 5 * 12) / 4)
                                    .foregroundColor(.white)
                                    .background(boton.colorDelBoton)
                                    .cornerRadius(self.AnchoDelBoton(botondelafuncion: boton))
                            }
                                                }
                          }
                                            }
            }.padding(.bottom)
            
              }
    }
    func capturar(tecla: CalcularBoton){
        switch tecla {
        case .suma, .resta, .multiplicacion, .division, .igual:
            if tecla == .suma{
                self.ejecutar = .suma
                self.numeroConsecutivo = Double(self.valorDeLaEtiqueta) ?? 0
            }
            else if tecla == .resta{
                self.ejecutar = .resta
                self.numeroConsecutivo = Double(self.valorDeLaEtiqueta) ?? 0
            }
            else if tecla == .multiplicacion{
                self.ejecutar = .multiplicacion
                self.numeroConsecutivo = Double(self.valorDeLaEtiqueta) ?? 0
                
            }
            else if tecla == .division{
                self.ejecutar = .divicion
                self.numeroConsecutivo = Double(self.valorDeLaEtiqueta) ?? 0
            }
            else if tecla == .igual{
                let  valorAlmacenado = self.numeroConsecutivo
                let valorActual = Double(self.valorDeLaEtiqueta) ?? 0
                switch self.ejecutar {
                case .suma: self.valorDeLaEtiqueta = "\(valorAlmacenado + valorActual)"
                case .resta: self.valorDeLaEtiqueta = "\(valorAlmacenado - valorActual)"
                case .multiplicacion: self.valorDeLaEtiqueta = "\(valorAlmacenado * valorActual)"
                case .divicion: self.valorDeLaEtiqueta = "\(valorAlmacenado / valorActual)"
                case .nada:
                    break
                }
            }
            
            if tecla != .igual{
                self.valorDeLaEtiqueta = "0"
            }
        case .ac: self.valorDeLaEtiqueta = "0"
            
        case .decimal, .sumaresta, .porcentaje:
            break
        default:
            let numero = tecla.titulo
            if self.valorDeLaEtiqueta == "0"{
                valorDeLaEtiqueta = numero
            }else{
                self.valorDeLaEtiqueta = "\(self.valorDeLaEtiqueta)\(numero)"
            }
        }
        
    }
    
    func AnchoDelBoton(botondelafuncion: CalcularBoton) -> CGFloat {
        if botondelafuncion == .cero {
            return (UIScreen.main.bounds.width - 5 * 12) / 4 * 2
            
        }
        return (UIScreen.main.bounds.width - 5 * 12) / 4
    }
    
    
    

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
    }
