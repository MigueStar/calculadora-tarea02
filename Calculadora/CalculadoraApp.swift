//
//  CalculadoraApp.swift
//  Calculadora
//
//  Created by Miguel Fernandez on 21/05/21.
//

import SwiftUI

@main
struct CalculadoraApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
